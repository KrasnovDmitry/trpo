(ns clj3-2)

(def elems_num 3)
(def threads_num 2)

(defn task [pred coll]
  ;(println "task")
  ;(println coll)
  (future
    (reduce
      (fn [filtered x]
        (if (pred x)
          (conj filtered x)
          filtered
          )
        )
      []
      coll
      )
    )
  )

(defn filter_thread [pred coll]
  ;(println "filter")
  ;(println coll)
  (reduce
    (fn [res part]
      (concat res @(task pred part) ) )
    []
    (partition-all elems_num coll)
    )
  )

(defn filter_parallel [pred coll]
  ;(println "partition")
  ;(println (partition-all
  ;           (int (Math/ceil (/ (count coll) threads_num)))
  ;           coll))
  (reduce
    (fn [res i] (concat res i))
    []
    (pmap
      #(filter_thread pred %)
      (partition-all
        (int (Math/ceil (/ (count coll) threads_num)))
        coll)
      )
    )
  )

(println (filter_parallel even? (range 23)))

