def combine_with_objs(v, objs)
    allowed_objs = objs - [v[-1]]
    return allowed_objs.map {|o|; v + [o]}
end

def all_combinations_reduce(objs, n)
    (1..n).reduce([[]]) {
        |combs| (combs.map {
            |v| combine_with_objs(v, objs)
        }).flatten(1)
    }
end

#objs = [24, 25, 31]
puts "Enter set of symbols:"
objs = gets.chomp.split("")
puts "objects=" + objs.to_s

#n=3
puts "Enter length (n):"
n = gets.chomp.to_i

puts "all combinations for length #{n}"
puts all_combinations_reduce(objs, n).to_s

