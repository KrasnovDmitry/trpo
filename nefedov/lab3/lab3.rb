require 'set'

"""class for storing authorization attempts information from auth.log files in Ubuntu OS and get some statistics from it"""
class AuthlogStat

    """regex for getting common line info"""
    @@logline_regex = /\A
                      (?<month>(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)) #month
                      \s{1,2}
                      (?<day>\d{1,2}) #day
                      \s
                      (?<time>\d{1,2}:\d{1,2}:\d{1,2}) #time in hh:mm:ss format)
                      \s
                      (?<host>[\w-]+) #hostname
                      \s
                      (?<content>.+) #log message content
                      \Z/x
         
    """regex for getting info about successful authorization"""           
    @@s_opened_regex = /\A
                       .+
                       session\sopened\sfor\suser\s
                       (?<user>\w+) #username
                       .*
                       \Z/x
    
    """regex for getting info about authorization failure"""                
    @@a_failure_regex = /\A
                        .+
                        authentication\sfailure;\s
                        .+
                        user=
                        (?<user>\w+) #username
                        .*
                        \Z/x
    
    """define AuthAttempt Struct type for storing attempts"""
    Struct.new("AuthAttempt", :month, :day, :time, :host, :is_successful, :user, :content)
    
    """define list variable for storing AuthAttempt"""
    @attempts

    """method reads log file and saves information about authorization attempts in @attempts list"""
    def read_authlog_info(file_path)
        @attempts = []
        File.open(file_path, "r") do |f|
            lnum = 0
            f.each_line do |line|
                lnum += 1
                md = line.match(@@logline_regex)
                if (md) then
                    content = md["content"]
                    md_opened = content.match(@@s_opened_regex)
                    if (md_opened) then
                        #puts("#{lnum}: date=#{md["date"]}, time=#{md["time"]}")
                        #puts("session opened for user=#{md_opened["user"]}")
                        a = Struct::AuthAttempt.new(md["month"], md["day"].to_i, md["time"], md["host"], true, md_opened["user"], content)
                        @attempts.push(a)
                    else
                        md_failed = content.match(@@a_failure_regex)
                        if (md_failed) then
                            #puts("#{lnum}: date=#{md["date"]}, time=#{md["time"]}")
                            #puts("auth failed for user=#{md_failed["user"]}")
                            a = Struct::AuthAttempt.new(md["month"], md["day"].to_i, md["time"], md["host"], false, md_failed["user"], content)
                            @attempts.push(a)
                        end
                    end
                end
            end
        end
    end
    
    """get list of all users who tried to authorize"""
    def get_users()
        users = [].to_set
        @attempts.each { |a| users.add(a.user) }
        return users
    end
    
    """get list of authorization attempts for some month and day"""
    def get_attempts(month, day)
        a_for_date = []
        @attempts.each { |a|
            if (month == a.month && day == a.day) then
                a_for_date.push(a)
            end
        }
        return a_for_date
    end
    
    """get amount of successful/failed authorization attempts for some user, month and day"""
    def get_results_stat(user, month, day)
        successful = 0
        failures = 0
        @attempts.each { |a|
            if (user == a.user && month == a.month && day == a.day) then
                a.is_successful ? successful += 1 : failures += 1
            end
        }
        return successful, failures
    end

end


authlog = AuthlogStat.new()
authlog.read_authlog_info("log.txt")

users = authlog.get_users()
puts("all users: #{users.to_a.to_s}")
puts

attempts_for_14_oct = authlog.get_attempts("Oct", 14)
puts("all attempts to authorize for 14 Oct (amount=#{attempts_for_14_oct.length}):")
attempts_for_14_oct.each { |a| puts("#{a.time} user=#{a.user}, content=#{a.content}") }
puts

successful, failures = authlog.get_results_stat("dminefyodov", "Oct", 25)
puts("25 Oct dminefyodov #{successful+failures} times tried to authorize")
puts("#{successful} successful attempts and #{failures} failures")

