(ns clj4)
(require 'clojure.test)

(defn constant [bool]
  {:pre [(boolean? bool)]}
  (list ::const bool)
  )

(defn constant? [expr]
  (= (first expr) ::const)
  )

(defn constant-value [c]
  (second c)
  )

(defn variable [name]
  {:pre [(keyword? name)]}
  (list ::var name)
  )

(defn variable? [expr]
  (= (first expr) ::var)
  )

(defn variable-name [v]
  (second v)
  )

(defn same-variables? [v1 v2]
  (and
    (variable? v1)
    (variable? v2)
    (= (variable-name v1)
       (variable-name v2)))
  )

(clojure.test/is (constant? (constant true)))
(clojure.test/is (= true (constant-value (constant true))))
(clojure.test/is (variable? (variable :x)))
(clojure.test/is (= :x (variable-name (variable :x))))
(clojure.test/is (same-variables?
           (variable :x)
           (variable :x)))
(clojure.test/is (not (same-variables?
                (variable :x)
                (variable :y))))

(defn args [expr]
  ;(println "args")
  ;(println (rest expr))
  (rest expr)
  )

(defn invert [expr]
  (list ::inv expr)
  )

(defn invert? [expr]
  (= (first expr) ::inv)
  )

(defn subexpr [expr]
  {:pre [(invert? expr)]}
  (second expr)
  )

(defn conjunct [expr & rest]
  (cons ::conj (cons expr rest))
  )

(defn conjunct? [expr]
  (= (first expr) ::conj)
  )

(defn disjunct [expr & rest]
  (cons ::disj (cons expr rest))
  )

(defn disjunct? [expr]
  (= (first expr) ::disj)
  )

(defn implicat [exprl exprr]
  (cons ::impl (list exprl exprr))
  )

(defn implicat? [expr]
  (= (first expr) ::impl)
  )

(defn left [expr]
  {:pre [(implicat? expr)]}
  (second expr)
  )

(defn right [expr]
  {:pre [(implicat? expr)]}
  (nth expr 2)
  )

(declare bool-expr)
(def bool-rules
  (list
    [(fn [expr] (constant? expr))
     (fn [expr] expr)]
    [(fn [expr] (variable? expr))
     (fn [expr] expr)]
    [(fn [expr] (invert? expr))
     (fn [expr]
       (cond
         (constant? (subexpr expr)) ;invert constant value
           (constant (not (constant-value (subexpr expr))))
         (variable? (subexpr expr))
           expr
         (invert? (subexpr expr)) ;get rid of double inversions
           (bool-expr (subexpr (subexpr expr)))
         (disjunct? (subexpr expr))
           (bool-expr (apply conjunct (map #(bool-expr (invert %)) (args (subexpr expr))) ) )
         (conjunct? (subexpr expr))
           (bool-expr (apply disjunct (map #(bool-expr (invert %)) (args (subexpr expr))) ) )
         (implicat? (subexpr expr))
           (bool-expr (conjunct (left (subexpr expr)) (invert (right (subexpr expr)))))
         )
         )]
    [(fn [expr] (implicat? expr))
     (fn [expr]
       (bool-expr (disjunct
                    (invert (left expr))
                    (right expr)))
       )]
    [(fn [expr] (disjunct? expr))
     (fn [expr]
       (apply disjunct (mapcat #(if (disjunct? %)
                                 (map (fn [arg] (bool-expr arg)) (args %) ) ;;flatten (a || (b || c))
                                 (list (bool-expr %))
                                 )
                               (args expr))) )]
    [(fn [expr] (conjunct? expr))
     (fn [expr]
       (let [flattened (mapcat #(if (conjunct? %)
                                  (map (fn [arg] (bool-expr arg)) (args %) ) ;;flattern (a && (b && c))
                                  (list (bool-expr %))
                               )
                               (args expr) )
             found_disjunction (first (filter (fn [arg] (disjunct? arg)) flattened) )
             ]
         (if (nil? found_disjunction)
           flattened
           (apply disjunct
                  (map
                    (fn [arg]
                      (apply conjunct (cons arg (filter
                                                  (fn [conjunction] (not (identical? conjunction found_disjunction)) )
                                                  flattened
                                                  )))
                      )
                    (args found_disjunction))
                  )
           )
         )
        )]
    )
  )

(defn bool-expr [expr]
  ((some (fn [rule]
           (if ((first rule) expr)
             (second rule)
             false))
         bool-rules)
    expr))

(println (bool-expr (invert (constant true))))
(println (bool-expr (invert (invert (constant true))) ))
(println (bool-expr (invert (invert (invert (constant true)))) ))

(println (bool-expr (variable :x)) )
(println (bool-expr (invert (variable :x))))
(println (bool-expr (invert (invert (variable :x))) ))
(println (bool-expr (invert (invert (invert (variable :x)))) ))

(println (bool-expr (implicat (constant true) (constant true))) )
(println (bool-expr (implicat (variable :x) (variable :y))) )

(println (disjunct (variable :x) (constant true)))
(println (disjunct? (disjunct (variable :x) (constant true))) )

(println (invert (disjunct (variable :x) (variable :y) (variable :z))))
(println (bool-expr (invert (disjunct (variable :x) (variable :y) (variable :z)))) )

(println (bool-expr (conjunct (variable :x) (variable :y) (variable :z))))
(println (bool-expr (disjunct (variable :x) (variable :y) (variable :z))))
(println (bool-expr (conjunct (variable :x) (disjunct (variable :x) (variable :y)) (variable :z))))
(println (bool-expr (disjunct (variable :x) (disjunct (variable :x) (variable :y)) (variable :z))))

(println (bool-expr (conjunct (disjunct (variable :a) (variable :b) (variable :c))
                              (disjunct (variable :d) (variable :e))
                              (variable :f) ) ))

(println "--------------------------")

;(declare substitute)
;(def subs-rules
;  (list
;    [(fn [expr list_of_values] (constant? expr))
;     (fn [expr list_of_values] expr)]
;    [(fn [expr list_of_values] (variable? expr))
;     (fn [expr list_of_values]
;       list_of_values
;       ;(nth
;       ;  (filter (fn [var] (same-variables? expr (first var) ) ) list_of_values)
;       ;  1)
;       )
;     ]
;  ))
;
;(defn substitute [expr list_of_values]
;  ((some (fn [rule]
;           (if ((first rule) expr list_of_values)
;             (second rule)
;             false))
;         subs-rules)
;    expr list_of_values))
;
;(println (substitute (constant true) () ))
;(println (substitute (variable :x) ((variable :x) (constant true)) ))
