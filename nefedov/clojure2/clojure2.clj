(ns clj2)

;;get square of trapeze using its bases (a, b) and height (h)
(defn trapeze_sqr [a, b, h]
  (/ (* (+ a b) h) 2.0)
  )

;;function which gets function to be integrated (f) and a step size (step)
(defn integral [f step]
  ;;function which computes integral of function "f" from x to 0 with step size "step"
  (defn int_sum [x]
    (if (> x 0)
      (+ (int_sum (dec x) )
         (trapeze_sqr
           (f (* x step))
           (f (* (dec x) step))
           step
           )
         )
      0
      )
    )
  ;;this function is used for iterating with integer counter
  #(int_sum (int (/ % step)) )
  )

;;function which gets function to be integrated (f) and a step size (step) using memoization
(defn integral_memo [f step]
  ;;memoized function which computes integral of function "f" from x to 0 with step size "step"
  (let [mem_int_sum
        (memoize
          (fn int_sum [f2 x]
            (println (str "called for " x))
            (if (> x 0)
              (+ (f2 f2 (dec x) )
                 (trapeze_sqr
                   (f (* x step))
                   (f (* (dec x) step))
                   step
                   )
                 )
              0
              )
            )
          )
        mem2 (partial mem_int_sum mem_int_sum)]
  ;;this function is used for iterating with integer counter
  #(mem2 (int (/ % step))))
  )

;;func to integrate
(defn myfunc [x]
  (/ (+ x 2) (+ (* x x) (- 20 x)) )
  )

;;ensure that it works
(println "check integral from 0 to 3 with step=0.5, result should be 7.5")
( println ((integral (fn [i] (+ i 1)) 0.5) 3) )
( println ((integral_memo (fn [i] (+ i 1)) 0.5) 3) )
(println)

(println "integrating function")
(println "without memoization:")
(time ((integral (fn [i] (myfunc i)) 0.1) 10))
(time ((integral (fn [i] (myfunc i)) 0.1) 10))
(time ((integral (fn [i] (myfunc i)) 0.1) 10))
(time ((integral (fn [i] (myfunc i)) 0.1) 11))
(println "with memoization:")
(let [intmem (integral_memo myfunc 0.1)]
  (time (intmem 10))
  (time (intmem 10))
  (time (intmem 10))
  (time (intmem 11))
  (time (intmem 11))
  )


;;https://stackoverflow.com/questions/12955024/recursion-inside-let-function
