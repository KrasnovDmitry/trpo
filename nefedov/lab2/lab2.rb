class MyArray

    @@task_size = 100_000
    @@threads_amount = 4
    
    @a = []
    
    def initialize(a)
        @a = a
    end
    
    def parallel(init_value)
        tasks_amount = (@a.length/@@task_size.to_f).ceil
        puts "len=#{@a.length}, tasks_amount=#{tasks_amount}"
        next_task_num = 0
        tasks_expired = false
        mutex = Mutex.new
        tasks_res = Array.new(tasks_amount, init_value)
        
        ( (0..@@threads_amount-1).map {|i| 
            Thread.new(i) { |thread_num|
                task_num = 0
                while (true)
                    mutex.synchronize {
                        if (next_task_num < tasks_amount)
                            task_num = next_task_num
                            next_task_num = next_task_num + 1
                        else
                            tasks_expired = true
                        end
                    }
                    break if tasks_expired
                    
                    last_item = task_num*@@task_size + @@task_size - 1
                    last_item = last_item < @a.length ? last_item : @a.length - 1
                    tasks_res[task_num] = @a[task_num*@@task_size .. last_item].reduce(init_value) {|res, e| yield(res, e)}
                    puts "thread #{thread_num} task #{task_num}, range:(#{task_num*@@task_size}..#{last_item})"
                        #+ ", tasks_res[#{task_num}] = " + tasks_res[task_num].to_s
                end
            }
        }).map(&:join)
        return tasks_res
    end
    
    def map
        ( parallel([]) {|res, e| res += [yield(e)]} ).reduce([]) {|res, e| res += e}
    end

    def any?
        ( parallel(false) {|res, e| res |= yield(e)} ).reduce(false) {|res, e| res |= e}
    end
    
    def all?
        ( parallel(true) {|res, e| res &= yield(e)} ).reduce(true) {|res, e| res &= e}
    end
    
    def select
        ( parallel([]) {|res, e| yield(e) ? res += [e] : res} ).reduce([]) {|res, e| res += e}
    end

end

n = 700_001
a4 = Array.new(n) { rand(0..9) }
ma4 = MyArray.new(a4)
puts "array of #{n} elements was generated"

puts 'map'
sum4 = a4.reduce(0) {|res, e| res += e}
i=0
a5 = ma4.map {|e| e + 1}
sum5 = a5.reduce(0) {|res, e| res += e}
puts 'sum of original array elements = '+sum4.to_s+', sum after adding 1 to each element = '+sum5.to_s
puts

puts 'any?'
puts ma4.any? {|e| e > 8} #true
puts ma4.any? {|e| e > 9} #false
a4[4] = 10
puts ma4.any? {|e| e > 9} #true
puts ma4.any? {|e| e > 10} #false
puts

puts 'all?'
puts ma4.all? {|e| e > -1} #true
puts ma4.all? {|e| e > 0} #false
puts

puts 'select'
puts ma4.select {|e| e == 11}.to_s # 0 elements
puts ma4.select {|e| e == 10}.to_s # 1 element
a4[9] = 10
puts ma4.select {|e| e == 10}.to_s # 2 elements
puts

