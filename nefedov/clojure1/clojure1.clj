;;remove object from set of objects (objs) which was last in the current vector v
(defn remove_last [v objs] (remove #{(last v)} objs) )

;;returns combinations of the vector v with all objects from objs except last object of v
(defn combine_with_objs [v, objs] 
  (map #(concat v [%])
    (remove_last v objs) ) )

;;returns combinations of all vectors with all objects in objs without repeating two same objects in a row
(defn all_combs [vectors, objs]
  (mapcat #(combine_with_objs % objs) vectors)
)

;;returns all vectors of length n consisting out of objects objs without repeating two same objects in a row using reduce
(defn all_combs_reduce [objs, n]
  (reduce 
    (fn [combs, i] (all_combs combs objs))
    [[]]
    (range 0 n)
  )
)

;;returns all vectors of length n consisting out of objects objs without repeating two same objects in a row using tail recursion
(defn all_combs_recur [objs, n]
  (loop [i n combs [[]]]
    (if (= i 0)
      combs
      (recur (- i 1) (all_combs combs objs))
    )
  )
)

(println "reduce:" (all_combs_reduce [1 2 3] 3) )
(println "recurs:" (all_combs_recur [1 2 3] 3) )
(time (all_combs_reduce [1 2 3] 12) )
(time (all_combs_recur [1 2 3] 12) )
