package ru.nsu.krasnov.persistence.map;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class PersistenceHashMapTest {

    @Test
    public void testPutSuccess() {
        PersistenceHashMap<String, Integer> persistenceHashMap = new PersistenceHashMap<>();
        persistenceHashMap.put("one", 1);
        Assert.assertEquals((Integer) 1, persistenceHashMap.get("one"));
    }

    @Test
    public void testGetSuccess() {
        PersistenceHashMap<String, Integer> persistenceHashMap = new PersistenceHashMap<>();
        persistenceHashMap.put("one", 1);
        persistenceHashMap.put("two", 2);
        Assert.assertEquals((Integer) 1, persistenceHashMap.get("one"));
    }

    @Test
    public void testGetVersionSuccess() {
        PersistenceHashMap<String, Integer> persistenceHashMap = new PersistenceHashMap<>();
        persistenceHashMap.put("one", 1);
        persistenceHashMap.put("two", 2);
        persistenceHashMap.put("two", 5);
        Assert.assertEquals((Integer) 2, persistenceHashMap.get("two", 2));
        Assert.assertNotEquals((Integer) 2, persistenceHashMap.get("two"));
    }

    @Test
    public void testRemoveSuccess() {
        PersistenceHashMap<String, Integer> persistenceHashMap = new PersistenceHashMap<>();
        persistenceHashMap.put("one", 1);
        Assert.assertNotNull(persistenceHashMap);
        Assert.assertEquals((Integer) 1, persistenceHashMap.remove("one"));
    }

    @Test
    public void testSize() {
        PersistenceHashMap<String, Integer> persistenceHashMap = new PersistenceHashMap<>();
        persistenceHashMap.put("one", 1);
        persistenceHashMap.put("three", 3);
        Assert.assertEquals(2, persistenceHashMap.size());
    }

    @Test
    public void testSizeVersionSuccess() {
        PersistenceHashMap<String, Integer> persistenceHashMap = new PersistenceHashMap<>();
        persistenceHashMap.put("one", 1);
        persistenceHashMap.put("three", 3);
        persistenceHashMap.put("two", 2);
        Assert.assertEquals(2, persistenceHashMap.size(2));
    }
}