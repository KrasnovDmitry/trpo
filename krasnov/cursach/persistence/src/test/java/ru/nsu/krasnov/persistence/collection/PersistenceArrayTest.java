package ru.nsu.krasnov.persistence.collection;

import org.junit.Assert;
import org.junit.Test;

public class PersistenceArrayTest {

    @Test
    public void testGetSuccess() {
        PersistenceArray<Integer> array = new PersistenceArray<>();
        Integer index1 = array.get(0, 1);
        Assert.assertNull("The first element must be null", index1);
    }

    @Test
    public void testAddSuccess() {
        PersistenceArray<Integer> array = new PersistenceArray<>();
        Integer a = array.add(3);
        Assert.assertEquals("The first element must be 3", (Integer) 3, array.get(a, array.size() - 1));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testGetIndexException() {
        PersistenceArray<Integer> array = new PersistenceArray<>();
        array.get(11);
    }

    @Test
    public void testGetIndexSuccess() {
        PersistenceArray<Integer> array = new PersistenceArray<>();
        array.add(5);
        Integer a = array.get(10);
        Assert.assertEquals("The last element must be 5", (Integer) 5, a);
    }

    @Test
    public void testRemoveSuccess() {
        PersistenceArray<Integer> array = new PersistenceArray<>();
        Integer a = array.add(5);
        Assert.assertEquals("The last element must be 5",  (Integer) 5, array.get(a, array.size() - 1));
        array.remove();
        Assert.assertNotEquals("The last element is 5", (Integer) 5, array.get(a, array.size() - 1));

    }

    @Test
    public void testSizeSuccess() {
        PersistenceArray<Integer> array = new PersistenceArray<>();
        array.add(10);
        Assert.assertEquals("The size must be 11", 11, array.size());
    }

    @Test
    public void testSizeVersionSuccess() {
        PersistenceArray<Integer> array = new PersistenceArray<>();
        Integer prevSize = array.size();
        array.add(10);
        Assert.assertEquals("The sizes are different", prevSize, (Integer) array.size(0));
    }

    @Test
    public void testSetSuccess() {
        PersistenceArray<Integer> array = new PersistenceArray<>();
        array.add(1);
        Assert.assertEquals("The last element must be 1", (Integer) 1, array.get(array.size()-1));
        array.set(array.size() - 1, 2);
        Assert.assertNotEquals("The last element must be 2", (Integer) 1, array.get(array.size()-1));
    }

    @Test
    public void testGetCurrentVersionSuccess() {
        PersistenceArray<Integer> array = new PersistenceArray<>();
        Assert.assertEquals("The version must be 0", 0, array.getCurrentVersion());
        array.add(1);
        Assert.assertNotEquals("The version must be 1", 0, array.getCurrentVersion());
    }

    @Test
    public void testCreateArrayWithGivenCapacity() {
        PersistenceArray<Integer> array = new PersistenceArray<>(8);
        Assert.assertEquals("Size is 2", 8, array.size());
    }
}