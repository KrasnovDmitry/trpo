package ru.nsu.krasnov.persistence.collection;

import org.junit.Assert;
import org.junit.Test;
import ru.nsu.krasnov.exceptions.VersionNotExistsException;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

public class PersistenceLinkedListTest {

    @Test
    public void testGetVersionSuccess() {
        PersistenceLinkedList<Integer> linkedList = new PersistenceLinkedList<>();
        linkedList.add(5);
        Assert.assertEquals("The last element is 5", (Integer) 5, linkedList.get(1, linkedList.size(1) - 1));
    }

    @Test
    public void testAddSuccess() {
        PersistenceLinkedList<Integer> linkedList = new PersistenceLinkedList<>();
        linkedList.add(1);
        Assert.assertEquals("List has one element", 1, linkedList.size(1));
    }

    @Test
    public void testSizeVersionSuccess() {
        PersistenceLinkedList<Integer> linkedList = new PersistenceLinkedList<>(Arrays.asList(1, 2, 3));
        Assert.assertEquals("List has 3 elements", 3, linkedList.size(0));
    }

    @Test
    public void testSizeSuccess() {
        PersistenceLinkedList<Integer> linkedList = new PersistenceLinkedList<>();
        linkedList.add(1);
        linkedList.add(2);
        Assert.assertEquals("List includes 2 elements", 2, linkedList.size());
        Assert.assertEquals("First version included 1 element", 1, linkedList.size(1));
    }

    @Test
    public void testRemoveSuccess() {
        PersistenceLinkedList<Integer> linkedList = new PersistenceLinkedList<>();
        linkedList.add(1);
        linkedList.add(2);
        Assert.assertEquals("List includes 2 elements", 2, linkedList.size());
        linkedList.remove(1);
        Assert.assertNotEquals("List includes 1 element", 2, linkedList.size());
    }

    @Test
    public void testRemove() {
        PersistenceLinkedList<Integer> linkedList = new PersistenceLinkedList<>();
        linkedList.add(3);
        linkedList.add(4);
        linkedList.remove(1);
        Assert.assertEquals("List includes 2 elements", 2, linkedList.size());
    }

    @Test
    public void testSetSuccess() {
        PersistenceLinkedList<Integer> linkedList = new PersistenceLinkedList<>();
        linkedList.add(1);
        linkedList.add(2);
        Assert.assertEquals("The last element is 2", (Integer) 2, linkedList.get(2, linkedList.size() -1 ));
        linkedList.set(1, 3);
        Assert.assertNotEquals("The last element is not 2", (Integer) 2, linkedList.get(3, linkedList.size() - 1));
    }

    @Test
    public void testGetCurrentVersion() {
        PersistenceLinkedList<Integer> linkedList = new PersistenceLinkedList<>();
        Assert.assertEquals("List is empty", 0, linkedList.getCurrentVersion());
        linkedList.add(1);
        Assert.assertNotEquals("List had been changed", 0, linkedList.getCurrentVersion());

    }

    @Test(expected = VersionNotExistsException.class)
    public void testVersionNotExistException() {
        PersistenceLinkedList<Integer> linkedList = new PersistenceLinkedList<>();
        linkedList.size(1);
    }
}