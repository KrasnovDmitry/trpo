package ru.nsu.krasnov.persistence.map;

/**
 * Interface for all persistence maps
 *
 * @param <K> type of key elements
 * @param <V> type of value elements
 */
public interface PersistenceMap<K, V> {
    /**
     * Method gets the value to which the specified key is mapped, or null if this map contains no mapping for the key.
     *
     * @param key
     */
    V get(K key);
}
