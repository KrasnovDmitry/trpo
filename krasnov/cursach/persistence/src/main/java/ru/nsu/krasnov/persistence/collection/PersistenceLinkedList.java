package ru.nsu.krasnov.persistence.collection;

import ru.nsu.krasnov.exceptions.VersionNotExistsException;

import java.util.Collection;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * Persistence linked list
 *
 * @param <T> type of elements in collection
 */
public class PersistenceLinkedList<T> implements PersistenceList<T> {

    private int currentVersion;
    private NavigableMap<Integer, Integer> versionSizeMap;
    private NavigableMap<Integer, ListNode> headMap;
    private NavigableMap<Integer, ListNode> tailMap;

    /**
     * Constructs an empty persistent list.
     */
    public PersistenceLinkedList() {
        currentVersion = 0;
        versionSizeMap = new TreeMap<>();
        headMap = new TreeMap<>();
        tailMap = new TreeMap<>();
        versionSizeMap.put(0, 0);
    }

    /**
     * Constructs a persistent list from specified collection.
     *
     * @param c specified collection
     */
    public PersistenceLinkedList(Collection<T> c) {
        this();
        for (T obj : c) {
            add(obj, currentVersion);
        }
    }

    @Override
    public T get(int version, int index) {
        // validation
        checkVersion(version);
        checkSize(index, version);

        // Extracting element
        ListNode current = headMap.floorEntry(version).getValue();
        for (int i = 0; i < size(version); i++) {
            if (i == index)
                return current.getElement(version);
            current = current.getNext(version);
        }
        return null;
    }

    /**
     * Method adds element to the list.
     *
     * @param element element for adding
     * @return true if this collection changed as a result of the call
     */
    public boolean add(T element) {
        currentVersion++;
        return add(element, currentVersion);
    }

    /**
     * Method adds new element to the end of list
     *
     * @param element element
     * @return current version
     */
    private boolean add(T element, int version) {
        Integer currentSize = size(version);
        if (currentSize == 0) {
            ListNode node = new ListNode(null, null, version, element);
            headMap.put(version, node);
            tailMap.put(version, node);
            versionSizeMap.put(version, 1);
        } else {
            ListNode previousTailNode = tailMap.floorEntry(version).getValue();
            ListNode node = new ListNode(null, previousTailNode, version, element);
            previousTailNode.setNext(version, node);
            tailMap.put(version, node);
            versionSizeMap.put(version, currentSize + 1);
        }
        return true;
    }

    /**
     * Method removes first occurrence of the given element
     *
     * @param element given element
     * @return version after removing
     */
    public int remove(T element) {
        int newVersion = currentVersion + 1;
        ListNode current = headMap.floorEntry(currentVersion).getValue();
        for (int i = 0; i < size(); i++) {
            if (current.getElement(currentVersion).equals(element)) {
                ListNode previousElement = current.getPrevious(currentVersion);
                ListNode nextElement = current.getNext(currentVersion);
                if (previousElement != null) {
                    previousElement.setNext(newVersion, nextElement);
                } else {
                    headMap.put(newVersion, nextElement);
                }
                if (nextElement != null) {
                    nextElement.setPrevious(newVersion, previousElement);
                } else {
                    tailMap.put(newVersion, previousElement);
                }
                versionSizeMap.put(newVersion, size(currentVersion) - 1);
                break;
            }
            current = current.getNext(currentVersion);
        }
        currentVersion++;
        return currentVersion;
    }

    /**
     * Method replaces element for current version
     *
     * @param index   index
     * @param element new element
     * @return previous element
     */
    public T set(int index, T element) {
        checkSize(index, currentVersion);

        ListNode current = headMap.floorEntry(currentVersion).getValue();
        for (int i = 0; i < index; i++) {
            current = current.getNext(currentVersion);
        }
        T previousElement = current.getElement(currentVersion);
        currentVersion++;
        current.setElement(currentVersion, element);

        return previousElement;
    }

    /**
     * Method returns current version of array
     *
     * @return version
     */
    public int getCurrentVersion() {
        return currentVersion;
    }

    /**
     * Method returns size for current version
     *
     * @return size
     */
    public int size() {
        return size(currentVersion);
    }

    /**
     * Method returns size for given version
     *
     * @param version version
     * @return size
     */
    public int size(int version) {
        checkVersion(version);
        return versionSizeMap.floorEntry(version).getValue();
    }

    //======================================
    // Validation methods

    private void checkVersion(int version) {
        if (version > currentVersion || version < 0) {
            throw new VersionNotExistsException(version);
        }
    }

    private void checkSize(int index, int version) {
        if (index < 0 || index >= size(version))
            throw new IndexOutOfBoundsException(String.format("Index = %d does not exist", index));
    }

    //======================================
    // Inner node class

    /**
     * Persistence linked list node
     */
    private final class ListNode {
        private NavigableMap<Integer, T> versionedData;
        private NavigableMap<Integer, ListNode> versionedPrev;
        private NavigableMap<Integer, ListNode> versionedNext;

        /**
         * Constructor
         *
         * @param next     next element
         * @param previous previous element
         * @param version  version
         * @param element  node element
         */
        ListNode(ListNode next, ListNode previous, int version, T element) {
            versionedData = new TreeMap<>();
            versionedPrev = new TreeMap<>();
            versionedNext = new TreeMap<>();

            versionedData.put(version, element);
            versionedPrev.put(version, previous);
            versionedNext.put(version, next);
        }

        /**
         * Method returns next node for given version
         *
         * @param version version
         * @return next node
         */
        public ListNode getNext(int version) {
            return versionedNext.floorEntry(version).getValue();
        }

        /**
         * Method sets next node for given version
         *
         * @param version version
         */
        public void setNext(int version, ListNode next) {
            versionedNext.put(version, next);
        }

        /**
         * Method returns previous node for given version
         *
         * @param version version
         * @return next node
         */
        public ListNode getPrevious(int version) {
            return versionedPrev.floorEntry(version).getValue();
        }

        /**
         * Method sets previous node for given version
         *
         * @param version version
         */
        public void setPrevious(int version, ListNode prev) {
            versionedPrev.put(version, prev);
        }

        /**
         * Method gets node's element for given version
         *
         * @param version version
         * @return node's element
         */
        public T getElement(int version) {
            return versionedData.floorEntry(version).getValue();
        }

        /**
         * Method sets node's element for given version
         *
         * @param version version
         */
        public void setElement(int version, T obj) {
            versionedData.put(version, obj);
        }
    }
}
