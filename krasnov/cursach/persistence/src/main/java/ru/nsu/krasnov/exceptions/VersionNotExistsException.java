package ru.nsu.krasnov.exceptions;

/**
 * Version not exists exception
 */
public class VersionNotExistsException extends RuntimeException {

    /**
     * Construcrot
     *
     * @param version given version
     */
    public VersionNotExistsException(int version) {
        super(String.format("Version %d does not exists", version));
    }
}
