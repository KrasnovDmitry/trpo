package ru.nsu.krasnov.persistence.map;

import ru.nsu.krasnov.exceptions.VersionNotExistsException;

import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * Persistence hash map
 *
 * @param <K> type of key elements
 * @param <V> type of value elements
 */
public class PersistenceHashMap<K, V> implements PersistenceMap<K, V> {
    private int currentVersion = 0;
    private Map<Integer, Integer> versionSizeMap;
    private Map<K, MapNode> innerMapData;

    /**
     * Constructs an empty persistent map.
     */
    public PersistenceHashMap() {
        currentVersion = 0;
        versionSizeMap = new HashMap<>();
        innerMapData = new HashMap<>();
        versionSizeMap.put(currentVersion, 0);
    }

    /**
     * Method puts value into the map for given key
     *
     * @param key   key
     * @param value value
     * @return previous value for given key,
     * or null if there was nor value for given key
     */
    public V put(K key, V value) {
        V oldValue = null;
        MapNode node = innerMapData.get(key);
        int newVersion = currentVersion + 1;
        if (null == node) {
            innerMapData.put(key, new MapNode(value, newVersion));
            int currSize = versionSizeMap.get(currentVersion);
            versionSizeMap.put(newVersion, currSize + 1);
        } else {
            oldValue = node.getElement(currentVersion);
            node.setElement(newVersion, value);
        }
        currentVersion++;
        return oldValue;
    }

    /**
     * Method returns value for given key and version
     *
     * @param key     the key whose associated value is to be returned
     * @param version version of this map
     * @return the value to which the given key is mapped, or null if this version of map contains no mapping for the key
     */
    public V get(K key, int version) {
        checkVersion(version);
        if (!innerMapData.containsKey(key))
            return null;
        MapNode node = innerMapData.get(key);
        if (!node.isRemoved(version)) {
            return node.getElement(version);
        }
        return null;
    }

    /**
     * Method returns value for given key
     *
     * @param key the key whose associated value is to be returned
     * @return the value to which the given key is mapped, or null if current version of map contains no mapping for the key
     */
    @Override
    public V get(K key) {
        return get(key, currentVersion);
    }

    /**
     * Method removes the mapping for a key from this map if it is present.
     *
     * @param key key
     * @return removed value, or null if not exists
     */
    public V remove(K key) {
        V oldValue = null;
        MapNode node = innerMapData.get(key);
        int newVersion = currentVersion + 1;
        if (null != node) {
            oldValue = node.getElement(currentVersion);
            node.removeObject(newVersion);
            int currSize = versionSizeMap.get(currentVersion);
            versionSizeMap.put(newVersion, currSize - 1);
        }
        currentVersion++;
        return oldValue;
    }

    /**
     * Method returns the number of elements in the given version of this map.
     *
     * @param version version of this map
     * @return number of elements in the specified version of this map.
     */
    public int size(int version) {
        checkVersion(version);
        return versionSizeMap.get(version);
    }

    /**
     * Returns the number of elements in the current version of this map.
     *
     * @return number of elements in the current version of this map.
     */
    public int size() {
        return size(currentVersion);
    }

    //======================================
    // Validation methods

    private void checkVersion(int version) {
        if (version > currentVersion || version < 0) {
            throw new VersionNotExistsException(version);
        }
    }

    //======================================
    // Inner node class

    private final class MapNode {

        private class InnerNode {
            private boolean isRemoved;
            private V element;

            InnerNode(V element) {
                this.element = element;
                isRemoved = false;
            }

            InnerNode(V element, boolean removed) {
                this.element = element;
                isRemoved = removed;
            }

            V getElement() {
                return element;
            }

            boolean isRemoved() {
                return isRemoved;
            }
        }

        private NavigableMap<Integer, InnerNode> versionedData;


        MapNode(V element, int version) {
            versionedData = new TreeMap<>();
            setElement(version, element);
        }

        private V getElement(int version) {
            return versionedData.floorEntry(version).getValue().getElement();
        }

        private void setElement(int version, V element) {
            versionedData.put(version, new InnerNode(element));
        }

        private void removeObject(int version) {
            versionedData.put(version, new InnerNode(null, true));
        }

        private boolean isRemoved(int version) {
            return versionedData.floorEntry(version) == null
                    || versionedData.floorEntry(version).getValue().isRemoved();
        }
    }
}
