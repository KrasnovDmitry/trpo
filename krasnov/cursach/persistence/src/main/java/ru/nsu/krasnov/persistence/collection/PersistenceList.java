package ru.nsu.krasnov.persistence.collection;

/**
 * Persistence list.
 *
 * @param <T> type of elements in collection
 */
public interface PersistenceList<T> extends PersistenceCollection<T> {

    /**
     * Methods returns element for given index and collection version
     *
     * @param version collection version
     * @param index   element index
     * @return element for given index if exists
     */
    T get(int version, int index);
}
