package ru.nsu.krasnov.persistence.collection;

import ru.nsu.krasnov.exceptions.VersionNotExistsException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * Persistence array
 *
 * @param <T> type of elements in collection
 */
public class PersistenceArray<T> implements PersistenceList<T> {

    private final static int DEFAULT_ARRAY_SIZE = 10;
    private int currentVersion;
    private Map<Integer, Integer> versionSizeMap;
    private ArrayList<NavigableMap<Integer, T>> innerArrayData;

    /**
     * Default constructor, constructs the innerArrayData with size = 10
     */
    public PersistenceArray() {
        currentVersion = 0;
        innerArrayData = new ArrayList<>();
        for (int i = 0; i < DEFAULT_ARRAY_SIZE; i++) {
            TreeMap<Integer, T> elementVersionMap = new TreeMap<>();
            elementVersionMap.put(currentVersion, null);
            innerArrayData.add(elementVersionMap);
        }
        versionSizeMap = new HashMap<>();
        versionSizeMap.put(currentVersion, innerArrayData.size());
    }

    /**
     * Constructs an empty list with the specified initial capacity.
     *
     * @param initialCapacity the initial capacity of the list
     * @throws IllegalArgumentException if the specified initial capacity
     *                                  is negative
     */
    public PersistenceArray(int initialCapacity) {
        currentVersion = 0;
        innerArrayData = new ArrayList<>(initialCapacity);
        for (int i = 0; i < initialCapacity; i++) {
            TreeMap<Integer, T> elementVersionMap = new TreeMap<>();
            elementVersionMap.put(currentVersion, null);
            innerArrayData.add(elementVersionMap);
        }
        versionSizeMap = new HashMap<>();
        versionSizeMap.put(currentVersion, innerArrayData.size());
    }

    /**
     * Methods adds the element to end of innerArrayData
     *
     * @param element current element
     * @return version after adding element
     */
    public int add(T element) {
        Integer arraySize = versionSizeMap.get(currentVersion);
        if (innerArrayData.size() <= arraySize) {
            innerArrayData.add(new TreeMap<>());
        }
        currentVersion++;
        innerArrayData.get(arraySize).put(currentVersion, element);
        versionSizeMap.put(currentVersion, arraySize + 1);
        return currentVersion;
    }

    /**
     * Method removes last element from innerArrayData
     *
     * @return version after removing object
     */
    public int remove() {
        Integer arraySize = versionSizeMap.get(currentVersion);
        if (arraySize == 0) {
            throw new ArrayIndexOutOfBoundsException("Array is empty, can not remove element.");
        }
        currentVersion++;
        versionSizeMap.put(currentVersion, arraySize - 1);
        return currentVersion;
    }

    /**
     * Method gets element for given version and index
     *
     * @param version collection version
     * @param index   element index
     * @return element
     * @throws {@link VersionNotExistsException}, {@link ArrayIndexOutOfBoundsException }
     */
    public T get(int version, int index) {

        if (version > currentVersion || version < 0) {
            throw new VersionNotExistsException(version);
        }

        if (versionSizeMap.get(version) <= index) {
            throw new ArrayIndexOutOfBoundsException(index);
        }

        if (innerArrayData.get(index).floorKey(version) == null) {
            throw new ArrayIndexOutOfBoundsException(index);
        }

        return innerArrayData.get(index).floorEntry(version).getValue();
    }

    /**
     * Method return element for given index and current version
     *
     * @param index index
     * @return element
     */
    public T get(int index) {
        return get(currentVersion, index);
    }

    /**
     * Method returns array size for given version
     *
     * @param version array version
     * @return array size
     * @throws {@link VersionNotExistsException}
     */
    public int size(int version) {
        if (version > currentVersion || version < 0) {
            throw new VersionNotExistsException(version);
        }
        return versionSizeMap.get(version);
    }

    /**
     * Method returns array size for current version
     *
     * @return array size
     */
    public int size() {
        return size(currentVersion);
    }

    /**
     * Method replaces element for current version
     *
     * @param index index
     * @param newEl new element
     * @return previous element
     */
    public T set(int index, T newEl) {
        Integer versionSize = versionSizeMap.get(currentVersion);
        if (versionSize <= index) {
            throw new ArrayIndexOutOfBoundsException(index);
        }
        T previousElement = innerArrayData.get(index).floorEntry(currentVersion).getValue();
        currentVersion++;
        innerArrayData.get(index).put(currentVersion, newEl);
        versionSizeMap.put(currentVersion, versionSize);
        return previousElement;
    }

    /**
     * Method returns current version of array
     *
     * @return version
     */
    public int getCurrentVersion() {
        return currentVersion;
    }
}
