package ru.nsu.krasnov.persistence.collection;

/**
 * Interface for all persistence collections
 *
 * @param <T> type of elements in collection
 */
public interface PersistenceCollection<T> {
}
