rx = /
    ^
    (?<date>\d{4}\-\d{2}\-\d{2}\s\d{2}:\d{2}:\d{2}\,\d{3})  #Date and time
    \s+
    (?<thread>\[.*\])                                       #Thread
    \s+
    (?<debugL>\w*)                                          #Debug level
    \s+
    (?<class>[\w\.]*)                                       #Class
    \s*-
    (?<msg>.*)                                              #Message
    $
    /x

arg = ARGV[2]

rx2 = /
      (.*#{arg}.*)    #find string with arg
      /x



File.open( ARGV[0], 'r') do |f|
    File.open( ARGV[1], 'w') do |file|
      f.each_line do |line|
        if arr = line.match(rx)
            if arr[:msg].match(rx2)
              print line
              file.write(line)
            end
        end
      end
    end

end