(ns lab6.core
  (:gen-class))

(defn simpleFunction [x]
  5)

(defn lazyIntegral [func step]
  (let [seqIntegral
        ((fn recurIntegral [previousSeq x]
           (lazy-seq
            (cons previousSeq (recurIntegral (+ previousSeq (* step (func x))) (+ x step))))) 0 0)]
    (fn [x] (nth seqIntegral (/ x step)))))

(let [integral (lazyIntegral simpleFunction 0.2)]
  (println (time (integral 3)))
  (println (time (integral 2)))
  (println (time (integral 1)))
  (println (time (integral 4)))
  (println (time (integral 3))))