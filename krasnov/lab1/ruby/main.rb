arr = ['a', 'b', [[:foo]]]
# [[:foo]]
n = 3

# permutation without recursion or cycle
def permutation(n ,arr)
  (1..n).reduce([[]]){
      |f|
    arr.map{|a| f.select{|c| !c.any?{|e| e==a}}.map{|c| c.select{|i| i}.push(a)}}.flatten(1)
  }
end

puts "Permutation with using only map and select:"
start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
first_permutation = permutation(n, arr)
end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
print first_permutation; puts
delta = (end_time - start_time) * 1000
puts "Time spent: %s milliseconds" % delta.to_s

# permutation with recursion
def recursion_permutation(element, permut_size, permut, input_arr, output_arr)
  if element != nil
    permut.push(element)
    input_arr.delete(element)
  end
  if permut.size < permut_size
    input_arr.map{|a| recursion_permutation(a, permut_size, permut.map{|e| e}, input_arr.map{|e| e}, output_arr)}
  else
    output_arr.push(permut)
  end
end

out = []

start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
puts "Recursion permutation:"
recursion_permutation(nil, n, [], arr, out )
end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
print out; puts
delta = (end_time - start_time) * 1000
puts "Time spent: %s milliseconds" % delta.to_s
