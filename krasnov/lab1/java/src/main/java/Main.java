import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // Input data
        int n = 3;
        List<String> arr = Arrays.asList("a", "b", "c");

        // Non recursive permutations
        List<List<String>> permutations = new ArrayList<>();
        make_permutation(n, null, new ArrayList<>(), new ArrayList<>(arr), permutations);
        System.out.println(permutations);

        // Recursive permutations
        List<List<String>> permutations_recursive = new ArrayList<>();
        make_permutation_recursive(null, n, new ArrayList<>(), new ArrayList<>(arr), permutations_recursive);
        System.out.println(permutations);
    }


    /**
     * Recursive variant for permutation
     *
     * @param n            size of permutation
     * @param str          input param
     * @param permutation  permutation
     * @param arr          input arr
     * @param permutations end variant's for permutations
     */
    private static void make_permutation_recursive(String str, int n, List<String> permutation, List<String> arr, List<List<String>> permutations){
        if(str != null){
            permutation.add(str);
            arr.remove(str);
        }
        if (permutation.size() < n) {
            arr.stream().peek((a) -> {
                make_permutation_recursive(a, n, new ArrayList<>(permutation), new ArrayList<>(arr), permutations);
            });
        } else {
            permutations.add(permutation);
        }
    }

    /**
     * Cycle variant for permutation
     *
     * @param n            size of permutation
     * @param str          input param
     * @param permutation  permutation
     * @param arr          input arr
     * @param permutations end variant's for permutations
     */
    private static void make_permutation(int n, String str, List<String> permutation, List<String> arr, List<List<String>> permutations) {
        if(str != null){
            permutation.add(str);
            arr.remove(str);
        }
        if (permutation.size() < n) {
            for (String s : arr) {
                make_permutation(n, s, new ArrayList<>(permutation), new ArrayList<>(arr), permutations);
            }
        } else {
            permutations.add(permutation);
        }
    }
}
