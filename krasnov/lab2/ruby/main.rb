# Работу с потоками в отдельную функцию           +
# убрать внешний резалт                           -
# флаг порядка                                    -
# степень параллелизма                            +
# если нал, то ддоставать кол-во ядер             +
# расширение класса Array добавить asParallel     +
# брать по ограниченному кол-ву потоков           +

load 'my_array.rb'
print Array.new([[[:foo]], 'b', 'c', 'a']).as_parallel(3).map{|a| a}
puts
print Array.new([[[:foo]], 'b', 'c', 'a']).as_parallel(3).select{|a| a!='a'}
puts
print Array.new([[[:foo]], 'b', 'c', 'a']).as_parallel(3).all?{|a| a!='l'}
puts
print Array.new([[[:foo]], 'b', 'c', 'a']).as_parallel(3).any?{|a| a=='1'}

# load 'my_fucking_array.rb'
#
# arr = ['a', 'b', 'c', 'd', 'e', 'f', 'j', 'k']
#
# myArr = MyFuckingArray.new(arr);
#
# puts "Not parallel map result:"
# start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
# result = arr.map{|a| a+a+a}
# end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
# print result; puts
# delta = (end_time - start_time) * 1000
# puts "Time spent: %s milliseconds" % delta.to_s; puts
#
# puts "Parallel map result:"
# start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
# result = myArr.map{|a| a+a+a}
# end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
# print result; puts
# delta = (end_time - start_time) * 1000
# puts "Time spent: %s milliseconds" % delta.to_s; puts
#
# puts "Not parallel select result:"
# start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
# result = arr.select{|a| a!='a'}
# end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
# print result; puts
# delta = (end_time - start_time) * 1000
# puts "Time spent: %s milliseconds" % delta.to_s; puts
#
# puts "Parallel select result:"
# start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
# result = myArr.select{|a| a!='a'}
# end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
# print result; puts
# delta = (end_time - start_time) * 1000
# puts "Time spent: %s milliseconds" % delta.to_s; puts
#
# puts "Not parallel any? result:"
# start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
# result = arr.any?{|a| a=='a'}
# end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
# print result; puts
# delta = (end_time - start_time) * 1000
# puts "Time spent: %s milliseconds" % delta.to_s; puts
#
# puts "Parallel any? result:"
# start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
# result = myArr.any?{|a| a=='a'}
# end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
# print result; puts
# delta = (end_time - start_time) * 1000
# puts "Time spent: %s milliseconds" % delta.to_s; puts
#
#
# puts "Not parallel all? result:"
# start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
# result = arr.all?{|a| a=='a'}
# end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
# print result; puts
# delta = (end_time - start_time) * 1000
# puts "Time spent: %s milliseconds" % delta.to_s; puts
#
# puts "Parallel all? result:"
# start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
# result = myArr.all?{|a| a=='a'}
# end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
# print result; puts
# delta = (end_time - start_time) * 1000
# puts "Time spent: %s milliseconds" % delta.to_s; puts
