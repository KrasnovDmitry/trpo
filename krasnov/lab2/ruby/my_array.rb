load 'thread_pool.rb'

class Array
  # Return array with parallel map, select, any?, all? functions
  def as_parallel(num_of_threads = OS.cpu_count, number_of_batches = 2)
    MyArray.new(self, num_of_threads, number_of_batches)
  end

  # Parallel extension of Array class
  class MyArray

    def initialize(arr, num_of_threads, number_of_batches)
      @arr = arr
      @num_of_threads = num_of_threads
    end

    def map
      pool = ThreadPool.new(@num_of_threads)
      batch_arr = []
      @arr.each_slice(@number_of_batches) { |a| batch_arr.push(a) }
      result_arr = []
      batch_arr.each_index do |index|
        pool.run do
          result_arr[index] = batch_arr.at(index).map { |a| yield(a) }
        end
      end
      pool.await_completion
      result_arr.flatten(1)
    end

    def select
      pool = ThreadPool.new(@num_of_threads)
      batch_arr = []
      @arr.each_slice(2) { |a| batch_arr.push(a) }
      result_arr = []
      batch_arr.each_index do |index|
        pool.run do
          result_arr[index] = batch_arr.at(index).select { |a| yield(a) }
        end
      end
      pool.await_completion
      result_arr.flatten(1)
    end

    def all?
      pool = ThreadPool.new(@num_of_threads)
      answer = true
      @arr.each_slice(3) do |batch|
        pool.run do
          batch.all? do |a|
            unless yield(a)
              answer = false
            end
          end
        end
      end
      pool.await_completion
      answer
    end

    def any?
      pool = ThreadPool.new(@num_of_threads)
      answer = false
      @arr.each_slice(3) do |batch|
        pool.run do
          batch.any? do |a|
            if yield(a)
              answer = true
            end
          end
        end
      end
      pool.await_completion
      answer
    end

  end

end


