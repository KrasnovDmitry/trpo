require 'os'
class ThreadPool
  def initialize(max_threads = 10)
    @pool = SizedQueue.new(max_threads)
    @running_threads = []
    @lock = Mutex.new
  end

  def run()
    @pool.push 1
    @running_threads << Thread.new do
      @lock.synchronize do
        yield if block_given?
      end
      @pool.pop
    end
  end

  def await_completion
    @running_threads.each {|thread| thread.join}
  end
end