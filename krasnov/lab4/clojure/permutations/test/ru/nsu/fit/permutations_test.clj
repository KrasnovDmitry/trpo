(ns ru.nsu.fit.permutations-test
  (:use clojure.test)
  (:use ru.nsu.fit.permutations))


(deftest fit.permutations-test
  (testing "Testing permutations"
    (is (= (permutation 3 '(1 2 3)) '((1 2 1) (3 2 1) (1 3 1) (2 3 1) (2 1 2) (3 1 2) (1 3 2) (2 3 2) (2 1 3) (3 1 3) (1 2 3) (3 2 3))))
    (is (= (permutation 2 '(1 2 3)) '((2 1) (3 1) (1 2) (3 2) (1 3) (2 3))))
    (is (= (permutation 1 '(1 2 3)) '((1) (2) (3))))
    (is (thrown? AssertionError (permutation 2 '())))
    (is (thrown? AssertionError (permutation -1 '(1 2 3))))))

(run-tests 'ru.nsu.fit.permutations-test)