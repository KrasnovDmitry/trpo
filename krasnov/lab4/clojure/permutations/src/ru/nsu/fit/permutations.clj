(ns ru.nsu.fit.permutations)

(defn rec_permutation [size permute_list input_list]
  "Tail recursion method makes permutation with given size from input list of chars"
  (if (> size 1)
    (recur (- size 1)
           (mapcat
             (fn[perm_el]
               (map
                 (fn [good_el]
                   (cons good_el perm_el))
                 (filter
                   (fn [el]
                     (not= el (first perm_el)))
                   input_list)))
             permute_list)
           input_list)
    permute_list))

(defn construct_list [el]
  (cons el '()))

(defn permutation [size input_list]
  "Method makes permutation with given size from input list of chars"
  (if (or (< size 0) (< (count input_list) 1))
    (throw (AssertionError. "Wrong input the size of permutation must be >=0"))
    (if (= size 0)
      '()
      (if (> size 1)
        (rec_permutation size (map construct_list input_list) input_list)
        (map construct_list input_list)))))



(println (permutation 2 `(1 2 3)))