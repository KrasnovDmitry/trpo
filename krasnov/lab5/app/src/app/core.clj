(ns app.core)

(defn simpleFunction [x]
  5)

(def recIntegralMem (memoize (fn [func step iters]
  (if  (> iters 0)
    (+ (* (func (* step iters)) step) (recIntegralMem func step (double (- iters 1))))
    0))))

(defn getIntegral [func step x]
  (recIntegralMem func step (/ x step)))

(println (time (getIntegral simpleFunction 0.2 3)))
(println (time (getIntegral simpleFunction 0.2 2)))
(println (time (getIntegral simpleFunction 0.2 1)))
(println (time (getIntegral simpleFunction 0.2 3)))
(println (time (getIntegral simpleFunction 0.2 2)))
(println (time (getIntegral simpleFunction 0.2 1)))