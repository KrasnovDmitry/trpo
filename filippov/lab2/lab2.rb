class Array

    class ThreadPool
        
        def initialize(max_threads)
            @pool = SizedQueue.new(max_threads)
            max_threads.times{ @pool << 1 }
            @mutex = Mutex.new
            @threads = []
        end
        
        def run()
            @pool.pop
            @mutex.synchronize {
                @threads << Thread.start {
                    yield
                    @pool << 1
                }
            }
        end

        def await_completion
            @threads.each {|thread| thread.join}
        end
        
    end
    
    def make_threads(n)
        threads = ThreadPool.new(n);
        
        self.each.with_index { |elem, i|
            threads.run{yield(elem, i)}
        }
        
        threads.await_completion;
    end

    def map1(n)
      out = []
      if block_given?
        make_threads(n){|elem, index| out[index] = yield(elem)}
        out.flatten(1)
      else
        out = nil
      end
   end
   
   def select1(n)
      out = []
      if block_given?
        make_threads(n){|elem, index| 
                                if(yield(elem)) 
                                    out << elem;
                                end
        }
                                
      else
        out = nil
      end
      out
   end 
   
   def all1?(n)
      out = true
      if block_given?
        make_threads(n){|elem| out = out && yield(elem)}
      else
        make_threads(n){|elem| out = out && elem}
      end
      out
   end
   
   def any1?(n)
      out = false
      if block_given?
        make_threads(n){|elem| out = out || yield(elem)}
      else
        make_threads(n){|elem| out = out || elem}
      end
      out
   end
   
end

a=Array.new([1,2,3,4,5,6,7,8,9,10])
print a.map1(3){|elem| elem*2}
puts
print a.select1(3){|elem| elem%2==0}
puts
print a.all1?(1){|elem| elem%2==0}
puts
print a.any1?(1){|elem| elem%2==0}