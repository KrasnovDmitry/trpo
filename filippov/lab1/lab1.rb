def getDifSym(str, arr) #string str, array of letters arr
    return arr.select{|elem| str.size==0 ? elem : elem != str[-1]} #array of symbols
end
 
def addDifSym(str, arr) #string str, array of letters arr
    return getDifSym(str, arr).map{|elem| str+[elem]} #array of string
end
 
def processArray(strArr, arr) #array of string strArr, array of letters arr
    strArr.flat_map{|str| addDifSym(str, arr)} 
     #array of string
end
 
def printRes(n, arr)
    print (1..n).reduce([[]]){|acc| acc = processArray(acc, arr)}
end
 
n = 3
arr = [1, 2, [[3,4]]]
print printRes(n, arr)
 