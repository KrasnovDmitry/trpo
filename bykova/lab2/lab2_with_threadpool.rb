class MyArray < Array
    
    @@thread_number=3
    
    class ThreadPool
        
        def initialize(max_threads)
            @pool = SizedQueue.new(max_threads)
            max_threads.times{ @pool << 1 }
            @mutex = Mutex.new
            @threads = []
        end
        
        def run()
            @pool.pop
            @mutex.synchronize {
                @threads << Thread.start {
                    yield
                    @pool << 1
                }
            }
        end

        def await_completion
            @threads.each {|thread| thread.join}
        end
        
    end
    
    
    def using_thread()
        pool = ThreadPool.new(@@thread_number);
        
        self.each.with_index { |element, index| 
                pool.run{yield(element, index)}
        }
        pool.await_completion;
    end
    
  
    def map()
        answer=(1..self.length).map{|i| []};
        using_thread(){|element, batch_index| 
            answer[batch_index] = yield(element);
        }
        return answer.flatten(1);
    end
   
   
    def select()
        answer = []
        min_index=1000000;
        using_thread(){|element| 
            if yield(element)
                answer << element;   
            end
        }
        return answer;
    end
 
    def all()
        answer=true;
        using_thread(){|element| 
            if !yield(element)
                answer=false;
            end
        }
        return answer;
    end
    
    def any()
        answer=false;
        using_thread(){|element| 
            if yield(element)
                answer=true;
            end
        }
        return answer;
    end
    
end


 # Tests:
 a=MyArray.new([1, 2, 3, 4, 5]);
 print a.map(){|i| i+1};
 puts
 print a.select(){|i| i>=4}
 puts
 print a.all() {|i| i!=1} 
 puts
 print a.any() {|i| i==4} 
 
 
 #Answer for the 2nd question:
 #No, because there is important the order of iterations in this case (for inject).