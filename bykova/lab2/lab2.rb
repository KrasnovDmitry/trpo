class MyArray < Array
    
    @@thread_number = 3;
    
    def using_thread()
        threads = []
        self.each_slice((self.length.to_f/@@thread_number).ceil.to_i).with_index { |batch, index|
                threads<< Thread.new{
                    batch.each{ |element| 
                        yield(element, index);
                    }
                }
        }
        threads.each{|thread| thread.join}
        self.flatten(1);
    end
    
  
    def map()
        answer=(1..@@thread_number).map{|i| []};
        using_thread(){|element, batch_index| 
            answer[batch_index] << yield(element);
        }
        return answer.flatten(1);
   end
   
   
    def select()
        answer = nil;
        min_index=1000000;
        using_thread(){|element, batch_index| 
            if yield(element) && batch_index<min_index
                answer=element;    
                min_index=batch_index;
            end
        }
        return answer;
    end
 
    def all()
        answer=true;
        using_thread(){|element, batch_index| 
            if !yield(element)
                answer=false;
            end
        }
    end
    
    def any()
        answer=false;
        using_thread(){|element, batch_index| 
            if yield(element)
                answer=true;
            end
        }
    end
   
end
 
 # Tests:
 a=MyArray.new([1, 2, 3, 4, 5]);
 print a.map(){|i| i+1};
 puts
 print a.select(){|i| i>=5}
 puts
 print a.all? {|i| i!=0} 
 puts
 print a.any? {|i| i==4} 
 
 #Answer for the 2nd question:
 #No, because there is important the order of iterations in this case (for inject).