(defn boolean? [x]
  (or (true? x) (false? x)))


;API ��� ��������:
  ;;���������� ���������
  (defn constant [value]
    {:pre [(boolean? value)]}
    (list ::const value))

  ;;�������� ���� ��� ���������
  (defn constant? [expr]
    (= (first expr) ::const))

  ;;��������� �������� ���������
  (defn constant-value [c]
    (second c))
  
  ;;�������� �������� ���������
  (defn reverse-value [c]
    (if (= true (constant-value c))
                    (constant false)
                    (constant true)))


;API ��� ����������:
  ;;���������� ����������
  (defn variable [name]
    {:pre [(keyword? name)]}
    (list ::var name))

  ;;�������� ���� ��� ����������
  (defn variable? [expr]
    (= (first expr) ::var))

  ;;��������� �������� ��� ����������
  (defn variable-name [v]
    (second v))

  ;;��������� ����������
  (defn same-variables? [v1 v2]
    (and
      (variable? v1)
      (variable? v2)
      (= (variable-name v1) (variable-name v2))))


;API ��� ���������� ��������:
  ;;���������� ����������
  (defn disjunction [expr & rest]
    (cons ::disj (cons expr rest)))

  ;;�������� ���� ��� ����������
  (defn disjunction? [expr]
    (= ::disj (first expr)))

  ;;���������� ����������
  (defn conjunction [expr & rest]
    (cons ::conj (cons expr rest)))

  ;;�������� ���� ��� ����������
  (defn conjunction? [expr]
    (= ::conj (first expr)))

  ;;���������� ���������
  (defn negation [expr]
    (cons ::not expr))

  ;;�������� ���� ��� ���������
  (defn negation? [expr]
    (= ::not (first expr)))
  
  ;;���������� ����������  
  (defn implication [expr-condition expr-consequence]
    (cons ::implies (cons expr-condition expr-consequence)))

    ;;�������� ���� ��� ����������
  (defn implication? [expr]
    (= ::implies (first expr)))

  (defn implication-condition [expr]
    (first (args expr)))

  (defn implication-consequence [expr]
    (second (args expr)))


;API ��� ��������� �������� ��� ����������:
;; ����� ������ ���������� � ���������
(defn get-disj [expr & rest]
  (first 
    (filter 
      #(disjunction? %) 
      (cons expr rest))))

(defn expression? [expr]
  (if (or (constant? expr) 
          (variable? expr) 
          (disjunction? expr) 
          (conjunction? expr) 
          (implication? expr) 
          (negation? expr))
      true
      false))

(defn not-expression? [expr]
  (if (not (expression? expr))
    true
    false))

;;��������� ���������� 
(defn one-arg? [expr]
  (= 1 (count (rest expr))))

(defn args [expr]
    (rest expr))


;���������� ������������ ���������� �����
  (declare dnf)

  ;;������ ���������� ��������������
  (def logic-rules
    (list
    ;;���������
      [(fn [expr] (constant? expr))
       (fn [expr] expr)]
    ;;����������
      [(fn [expr] (variable? expr))
       (fn [expr] expr)]
    ;;����������
      [(fn [expr] (disjunction? expr))
       (fn [expr]
          (apply disjunction
            (mapcat
              #(if (disjunction? %) 
                  (args %) 
                  (list %))
              (map #(dnf %) (args expr)))))]
    ;;����������
      [(fn [expr] (conjunction? expr))
       (fn [expr] 
            (let [update-args (mapcat
                              #(if (conjunction? %) 
                                (args %) 
                                (list %))
                              (args expr))
                first-disj (apply get-disj update-args)] 
            (if (nil? first-disj)
              (apply conjunction update-args)
              (dnf (apply disjunction
                (map 
                  (fn [arg]
                    (apply conjunction  
                      arg
                      (filter #(not (identical? first-disj %)) update-args)))
                  (args first-disj)))))))]
    ;;����������
      [(fn [expr] (implication? expr))
       (fn [expr]
          (let [first-arg (first (args expr))
                second-arg (rest (args expr))]
            (disjunction 
              (dnf (negation first-arg)) 
              (dnf second-arg))))]
    ;;���������
    [(fn [expr] (negation? expr))
       (fn [expr] 
          (let [sub-expr (args expr)]
            (cond
              (variable? sub-expr) 
                  expr
              (constant? sub-expr) 
                  (reverse-value sub-expr)
              (negation? sub-expr)
                  (let [sub-arg (args sub-expr)]
                    (if (or (variable? sub-arg) (constant? sub-arg)) 
                      sub-arg
                      (dnf sub-arg)))
              (disjunction? sub-expr) 
                  (apply conjunction
                      (map
                        #(dnf (negation %))
                        (args sub-expr)))
              (conjunction? sub-expr) 
                  (apply disjunction
                      (map 
                        #(dnf (negation %))
                        (args sub-expr)))
              (implication? sub-expr)
                  (dnf (negation (dnf sub-expr))))))]))        

  ;;������������ ���������� �����
  (defn dnf [expr]
    ((some (fn [rule]
          (if ((first rule) expr)
            (second rule)
            false))
      logic-rules)
      expr))


(dnf (disjunction (variable :x) (variable :x)))
(dnf (disjunction (disjunction (variable :x) (constant true)) (variable :z)))

;;����� ��� ���
(disjunction (variable :x) (variable :x))
(implication (variable :x) (variable :x))
(conjunction (variable :x) (variable :x))
(negation (variable :x))
(negation (negation (variable :x)))
(negation (constant false))
(negation (disjunction (variable :x) (variable :x)))

(dnf (disjunction (variable :x) (variable :x)))
(dnf (implication (variable :x) (variable :x)))
(dnf (conjunction (variable :x) (variable :x)))
(dnf (negation (variable :x)))
(dnf (negation (negation (variable :x))))
(dnf (negation (negation (negation (variable :x)))))
(dnf (negation (constant false)))
(dnf (negation (constant true)))

(dnf (negation (negation (negation (disjunction (variable :x) (constant true) (variable :z))))))
(dnf (disjunction (variable :x) (constant true) (variable :z)))
(dnf (disjunction (disjunction (variable :x) (constant true)) (variable :z)))
(dnf (negation (disjunction (variable :x) (constant true) (variable :z))))
(dnf (disjunction (variable :x) (variable :x) (disjunction (variable :y) (variable :x))))
(dnf (negation (disjunction (variable :x) (variable :x))))
(dnf (negation (conjunction (variable :x) (variable :x))))
(dnf (negation (disjunction (conjunction (variable :x) (variable :x)) (variable :x))))
(dnf (negation (conjunction (negation (disjunction (variable :x) (variable :y))) (variable :x))))
(dnf (negation (implication (variable :x) (variable :y))))
(dnf (disjunction (variable :x) (variable :x) (disjunction (disjunction (variable :y) (variable :z)) (variable :x))))
(dnf (disjunction (disjunction (variable :x) (variable :x)) (disjunction (variable :y) (variable :z)) (variable :x)))
(dnf (conjunction (conjunction (variable :a) (variable :b)) (conjunction (variable :c) (variable :d))))
(dnf (conjunction (disjunction (variable :a) (variable :b)) (variable :d)))
(dnf (conjunction (disjunction (variable :a) (variable :b)) (variable :d)))
(dnf (conjunction (disjunction (variable :a) (variable :b))  (disjunction (variable :a) (variable :b))))
(dnf (conjunction (disjunction (variable :a) (variable :b) (conjunction (variable :x) (variable :y)))  (disjunction (variable :a) (variable :b))))
(dnf (conjunction (variable :e) (conjunction (variable :a) (variable :b)) (conjunction (variable :c) (variable :d))))


(def values-table {:a (constant false)
                   :b (constant true)})

(defn has-value? [expr]
  (if (nil? (get values-table (variable-name expr)))
    false
    true))

(defn get-value [expr] 
  (let [value (get values-table (variable-name expr))]
        (if (nil? value)
          expr
          value)))


;�����������
  (declare sign-only)

  ;;������ ��������� �����������
  (def sign-rules
    (list
    ;;�� ��������
      [(fn [expr] (not-expression? expr))
       (fn [expr] (args expr))]
    ;;���������
      [(fn [expr] (constant? expr))
       (fn [expr] expr)]
    ;;����������
      [(fn [expr] (variable? expr))
       (fn [expr] (get-value expr))]
    ;;����������
      [(fn [expr] (disjunction? expr))
       (fn [expr]
          (apply disjunction
            (map #(sign-only %) (args expr))))]
    ;;����������
      [(fn [expr] (conjunction? expr))
       (fn [expr]
          (apply conjunction
            (map #(sign-only %) (args expr))))]
    ;;����������
      [(fn [expr] (implication? expr))
       (fn [expr]
          (let [first-arg (first (args expr))
                second-arg (rest (args expr))]
            (disjunction 
              (sign-only (negation first-arg)) 
              (sign-only second-arg))))]
    ;;���������
      [(fn [expr] (negation? expr))
       (fn [expr] 
          (let [sub-expr (args expr)]
            (cond
              (variable? sub-expr) 
                  (if (has-value? sub-expr) 
                    (reverse-value (get-value sub-expr))
                    expr)
              (constant? sub-expr) 
                  (reverse-value sub-expr))))]
    ;;������ ������
      [(fn [expr] (true))
       (fn [expr] 
          (sign-only (args expr)))]))        

  ;;������������ ���������� �����
  (defn sign-only [expr]
    ((some (fn [rule]
          (if ((first rule) expr)
            (second rule)
            false))
      sign-rules)
      expr))

(defn sign [expr] (sign-only (dnf expr)))

;����� ��� �����������
(sign (disjunction (variable :x) (variable :x)))
(sign (implication (variable :x) (variable :x)))
(sign (conjunction (variable :x) (variable :x)))
(sign (negation (variable :x)))
(sign (negation (negation (variable :x))))
(sign (negation (negation (negation (variable :x)))))
(sign (negation (constant false)))
(sign (negation (constant true)))
(sign (negation (negation (negation (disjunction (variable :x) (constant true) (variable :z))))))
(sign (disjunction (variable :x) (constant true) (variable :a)))
(sign (negation (disjunction (variable :x) (constant true) (variable :z))))

(sign (disjunction (variable :a) (variable :x) (disjunction (variable :y) (variable :x))))

(sign (disjunction (variable :a) (variable :x) (disjunction (variable :y) (variable :x))))
(sign (negation (negation (negation (disjunction (variable :x) (constant true) (variable :z))))))
(sign (disjunction (variable :x) (constant true) (variable :z)))
(sign (disjunction (disjunction (variable :x) (constant true)) (variable :z)))
(sign (negation (disjunction (variable :x) (constant true) (variable :z))))
(sign (disjunction (variable :x) (variable :x) (disjunction (variable :y) (variable :x))))
(sign (negation (disjunction (variable :x) (variable :x))))
(sign (negation (conjunction (variable :x) (variable :x))))
(sign (negation (disjunction (conjunction (variable :x) (variable :x)) (variable :x))))
(sign (negation (conjunction (negation (disjunction (variable :x) (variable :y))) (variable :x))))
(sign (negation (implication (variable :x) (variable :y))))
(sign (disjunction (variable :x) (variable :x) (disjunction (disjunction (variable :y) (variable :z)) (variable :x))))
(sign (disjunction (disjunction (variable :x) (variable :x)) (disjunction (variable :y) (variable :z)) (variable :x)))
(sign (conjunction (conjunction (variable :a) (variable :b)) (conjunction (variable :c) (variable :d))))
(sign (conjunction (disjunction (variable :a) (variable :b)) (variable :d)))
(sign (conjunction (disjunction (variable :a) (variable :b)) (variable :d)))
(sign (conjunction (disjunction (variable :a) (variable :b))  (disjunction (variable :a) (variable :b))))
(sign (conjunction (disjunction (variable :a) (variable :b) (conjunction (variable :x) (variable :y)))  (disjunction (variable :a) (variable :b))))
(sign (conjunction (variable :e) (conjunction (variable :a) (variable :b)) (conjunction (variable :c) (variable :d))))



(defn has-false? [expr] 
  (let [false-value (filter #(and (constant? %) (= (constant-value %) false)) (args expr))]
    (if (empty? false-value)
      false
      true)))

(defn has-true? [expr] 
  (let [true-value (filter #(and (constant? %) (= (constant-value %) true)) (args expr))]
    (if (empty? true-value)
      false
      true)))

(defn simplify-for-conj [expr] (filter #(not= % (constant true)) (distinct (args expr))))

  ;���������
  (declare simplify)

  ;;������ ��������� �����������
  (def simplify-rules
    (list
    ;;���������
      [(fn [expr] (constant? expr))
       (fn [expr] expr)]
    ;;����������
      [(fn [expr] (variable? expr))
       (fn [expr] (get-value expr))]
    ;;���������
      [(fn [expr] (negation? expr))
       (fn [expr] (get-value expr))]
    ;;����������
      [(fn [expr] (disjunction? expr))
       (fn [expr]
          (let [simple-expr (filter #(not= % (constant false)) (map #(simplify-only %) (args expr)))
                new-expr (apply disjunction (distinct (map #(simplify-only %) (args expr))))]
            (if (has-true? new-expr)
              (constant true)
              new-expr)))]
    ;;����������
      [(fn [expr] (conjunction? expr))
       (fn [expr]
          (if (has-false? expr) 
              (constant false)
              (let [simple-expr (simplify-for-conj expr)]
                (if (= 1 (count simple-expr)) 
                  simple-expr
                  (apply conjunction simple-expr)))))]))        

  ;;�������� �������
  (defn simplify-only [expr]
    ((some (fn [rule]
          (if ((first rule) expr)
            (second rule)
            false))
      simplify-rules)
      expr))

(defn dnf-with-simplify [expr] (simplify-only(sign expr)))


(simplify-only (disjunction (conjunction (variable :c) (variable :d) (constant true)) (conjunction (variable :x) (variable :x))))

(dnf-with-simplify (disjunction (conjunction (variable :c) (variable :d) (constant true)) (conjunction (variable :x) (variable :x))))


