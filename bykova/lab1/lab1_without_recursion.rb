def getAcceptObjects(letters, before)
    letters.select{|c| c!=before};
end

def getObjects(letters, n)
    (1..n).reduce([[]]){
        |answer, i| answer.map{
            |permutation| 
            getAcceptObjects(letters, permutation.length==0 ? nil : permutation[-1]).map{
                |obj| 
                permutation+[obj]
            }
        }.flatten(1)
    };
end


n=2;
letters=[:a, :b, :c];

getObjects(letters, n).each{
    |permutation| 
    print permutation 
    puts
}









