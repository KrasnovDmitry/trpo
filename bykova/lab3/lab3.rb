def countSMS(txt)
  rx = Regexp.new(/
    \d{2}\.\d{2}\.\d{4}\s       #data
    \d{1,2}:\d{2}:\d{2}\s+      #time
    .+                          #this text is not matter
    \ssms\s+[i,o]\s+            #sms i(o)
    (\d)+                       #sms number
    .+
    /x)
  numSMS=txt.scan(rx).flatten(1)
  return numSMS[0].to_i 
end

def countSeconds(txt)
  rx = Regexp.new(/
   \d{2}\.\d{2}\.\d{4}\s        #data
   \d{1,2}:\d{2}:\d{2}\s+       #time
   .+                           #this text is not matter
   Phone\.\s+                   #Phone.
   (\d+):(\d+)                   #minutes and seconds
   .+
   /x)
  time = txt.scan(rx).flatten(1);
  return time[0].to_i*60+time[1].to_i
end

seconds = File.read("new_logs.txt").split("\n").reduce(0){ |cntSeconds, line| 
cntSeconds=cntSeconds+countSeconds(line)
}
puts "Minutes = #{seconds/60}"

sms = File.read("new_logs.txt").split("\n").reduce(0){ |cntSMS, line| 
cntSMS=cntSMS+countSMS(line)
}
puts "SMS number = #{sms}"